﻿#region usings

using System;
using TrueFit.Extensions;

#endregion

namespace MvcTemplate.Domain.Entities
{
  public class User
  {
    public User()
    {
      CreateDate = DateTime.UtcNow.RoundToSecond();
    }

    public virtual int Id { get; set; }
    public virtual int Version { get; set; }
    public virtual DateTime CreateDate { get; set; }
    public virtual string Email { get; set; }
    public virtual string Salt { get; set; }
    public virtual string Password { get; set; }
    public virtual Guid? PasswordResetToken { get; set; }
    public virtual DateTime? PasswordResetTokenExpirationDate { get; set; }

    protected virtual bool Equals(User other)
    {
      return string.Equals(Email, other.Email);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      var other = obj as User;
      return other != null && Equals(other);
    }

    public override int GetHashCode()
    {
      return Email.GetHashCode();
    }

    public static bool operator ==(User left, User right)
    {
      return Equals(left, right);
    }

    public static bool operator !=(User left, User right)
    {
      return !Equals(left, right);
    }
  }
}