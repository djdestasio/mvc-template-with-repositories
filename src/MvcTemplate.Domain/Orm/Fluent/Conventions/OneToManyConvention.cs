#region usings

using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

#endregion

namespace MvcTemplate.Domain.Orm.Fluent.Conventions
{
  public class OneToManyConvention : IHasManyConvention
  {
    public void Apply(IOneToManyCollectionInstance instance)
    {
      instance.Key.Column(instance.EntityType.Name + "Id");
    }
  }
}