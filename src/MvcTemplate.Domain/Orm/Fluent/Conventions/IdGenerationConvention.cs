#region usings

using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

#endregion

namespace MvcTemplate.Domain.Orm.Fluent.Conventions
{
  public class IdGenerationConvention : IIdConvention
  {
    public void Apply(IIdentityInstance instance)
    {
      instance.GeneratedBy.HiLo("1000");
    }
  }
}