#region usings

using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

#endregion

namespace MvcTemplate.Domain.Orm.Fluent.Conventions
{
  public class TableNameConvention : IClassConvention
  {
    public void Apply(IClassInstance instance)
    {
      var pluralizer = PluralizationService.CreateService(new CultureInfo("en-US"));
      instance.Table(pluralizer.Pluralize(instance.EntityType.Name));
    }
  }
}