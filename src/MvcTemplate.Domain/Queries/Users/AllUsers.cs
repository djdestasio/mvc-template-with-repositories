#region usings

using System.Collections.Generic;
using MvcTemplate.Domain.Entities;
using NHibernate;

#endregion

namespace MvcTemplate.Domain.Queries.Users
{
  public class AllUsers : IQuery<IEnumerable<User>>
  {
    public IEnumerable<User> Execute(ISession session)
    {
      return session.QueryOver<User>().List();
    }
  }
}