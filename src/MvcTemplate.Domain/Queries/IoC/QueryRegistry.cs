﻿using StructureMap.Configuration.DSL;

namespace MvcTemplate.Domain.Queries.IoC
{
  public class QueryRegistry : Registry
  {
    public QueryRegistry()
    {
      For<IQueryExecutor>().Use<QueryExecutor>();
      SetAllProperties(sc => sc.OfType<IQueryExecutor>());
    }
  }
}
