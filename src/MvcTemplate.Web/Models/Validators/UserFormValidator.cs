﻿#region usings

using System.Collections.Generic;
using System.Web.Mvc;
using MvcTemplate.Domain.Queries;
using MvcTemplate.Domain.Queries.Users;
using TrueFit.Web.StructureMap.Validation;

#endregion

namespace MvcTemplate.Web.Models.Validators
{
  public class UserFormValidator : ModelValidator, IModelValidator<UserForm>
  {
    private readonly IQueryExecutor _queryExecutor;

    public UserFormValidator(ModelMetadata modelMetadata, ControllerContext controllerContext, IQueryExecutor queryExecutor)
      : base(modelMetadata, controllerContext)
    {
      _queryExecutor = queryExecutor;
    }

    public override IEnumerable<ModelValidationResult> Validate(object container)
    {
      if (Metadata.Model == null) yield break;

      var model = (UserForm) Metadata.Model;
      if (model.Id == null && string.IsNullOrEmpty(model.Password))
        yield return new ModelValidationResult {MemberName = "Password", Message = "The Password field is required."};

      if (model.Id == null && string.IsNullOrEmpty(model.ConfirmPassword))
        yield return new ModelValidationResult {MemberName = "ConfirmPassword", Message = "The Confirm Password field is required."};

      if (!string.IsNullOrEmpty(model.Email) && _queryExecutor.ExecuteQuery(new UserWithEmailAlreadyExists(model.Email, model.Id)))
        yield return new ModelValidationResult {MemberName = "Name", Message = "A product with this name already exists."};
    }
  }
}