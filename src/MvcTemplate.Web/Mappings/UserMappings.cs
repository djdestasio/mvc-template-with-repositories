#region usings

using System;
using AutoMapper;
using MvcTemplate.Domain.Entities;
using MvcTemplate.Web.Infrastructure.Authentication;
using MvcTemplate.Web.Models;
using NHibernate;

#endregion

namespace MvcTemplate.Web.Mappings
{
  public static class UserMappings
  {
    public static void Configure(IConfiguration cfg, Func<Type, object> createDependency)
    {
      var getSession = new Func<ISession>(() => (ISession) createDependency(typeof (ISession)));

      cfg.CreateMap<UserForm, User>()
        .ConstructUsing(uf => uf.Id == null ? new User() : getSession().Get<User>(uf.Id.Value))
        .ForMember(u => u.Version, ce => ce.Ignore())
        .ForMember(u => u.CreateDate, ce => ce.Ignore())
        .ForMember(u => u.Password, ce => ce.Ignore())
        .ForMember(u => u.Salt, ce => ce.Ignore())
        .AfterMap((form, user) =>
                    {
                      if (string.IsNullOrEmpty(form.Password)) return;
                      var passwordHasher = new PasswordHasher();
                      var salt = passwordHasher.GeneratePasswordSalt();
                      user.Password = passwordHasher.HashPassword(salt, form.Password);
                      user.Salt = salt;
                    });

      cfg.CreateMap<User, UserForm>()
        .ForMember(udf => udf.Password, ce => ce.Ignore())
        .ForMember(udf => udf.ConfirmPassword, ce => ce.Ignore());
    }
  }
}