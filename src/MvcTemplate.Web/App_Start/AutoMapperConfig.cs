#region usings

using System;
using AutoMapper;
using MvcTemplate.Web.App_Start;
using MvcTemplate.Web.Mappings;
using StructureMap;
using WebActivator;

#endregion

[assembly: PreApplicationStartMethod(typeof (AutoMapperConfig), "PreStart")]

namespace MvcTemplate.Web.App_Start
{
  public static class AutoMapperConfig
  {
    public static void PreStart()
    {
      Configure(ObjectFactory.GetInstance);
    }

    private static void Configure(Func<Type, object> createDependency)
    {
      Mapper.Initialize(
        cfg =>
          {
            cfg.ConstructServicesUsing(createDependency);
            UserMappings.Configure(cfg, createDependency);
          });
    }
  }
}