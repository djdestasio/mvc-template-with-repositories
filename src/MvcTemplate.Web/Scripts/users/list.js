﻿/*global define */

define(
  [
    'jquery',
    'lib/bootstrap/confirmDelete',
    'lib/bootstrap/ajaxFormModal',
    'lib/jquery/dataTables/dataTables',
    'lib/jquery/dataTables/defaults',
    'lib/jquery/dataTables/fnReloadAjax'
  ],
  function ($, confirmDelete) {
    var config;

    function openDialog(url, id) {
      $("#ajaxFormModalContainer").ajaxFormModal({
        contentRequestUrl: url,
        contentRequestData: { id: id },
        success: function () {
          $("#users").dataTable().fnReloadAjax();
        }
      });
    }

    function deleteUser(id) {
      confirmDelete.confirmDelete({
        url: config.deleteUrl,
        id: id,
        success: function () {
          $("#users").dataTable().fnReloadAjax();
        }
      });
    }

    function initializeUsersGrid() {
      $("#users").dataTable({
        sAjaxSource: config.listUrl,
        aoColumns: [
          { sTitle: "Email" },
          { sTitle: "Edit", sClass: "center edit" },
          { sTitle: "Delete", sClass: "center delete" },
          { bVisible: false }
        ],
        aoColumnDefs: [
          { bSortable: false, bSearchable: false, aTargets: [-1, -2, -3] },
          { sWidth: "60px", aTargets: [-2, -3] }
        ],
        fnCreatedRow: function (nRow, aData) {
          var id = aData[3];
          $("td.edit > a", nRow).click(function () { openDialog(config.editUrl, id); });
          $("td.delete > a", nRow).click(function () { deleteUser(id); });
        }
      });
    }

    return {
      initialize: function (moduleConfig) {
        config = $.extend({
          listUrl: '',
          addUrl: '',
          editUrl: '',
          deleteUrl: ''
        },
          moduleConfig);

        initializeUsersGrid();

        $('#addUser').click(function () {
          openDialog(config.addUrl);
        });
      }
    };
  });