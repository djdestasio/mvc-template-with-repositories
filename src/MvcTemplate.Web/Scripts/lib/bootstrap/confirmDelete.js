﻿/*global $, define */
// Copyright 2012, TrueFit, Inc.

define(['jquery'], function ($) {
  'use strict';

  return {
    confirmDelete: function (options) {
      $('#ajaxFormModalContainer').ajaxFormModal({
        contentRequestUrl: '/Shared/ConfirmDelete',
        contentRequestData: {
          url: options.url,
          id: options.id
        },
        success: options.success,
        error: function () {
          $('#ajaxFormModalContainer').ajaxFormModal('hide');
        }
      });
    }
  };
});
