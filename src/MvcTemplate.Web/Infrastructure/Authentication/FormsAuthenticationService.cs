﻿#region usings

using System.Globalization;
using System.Web.Security;

#endregion

namespace MvcTemplate.Web.Infrastructure.Authentication
{
  public class FormsAuthenticationService : IFormsAuthenticationService
  {
    public void LogOn(int userId, bool createPersistentCookie)
    {
      FormsAuthentication.SetAuthCookie(userId.ToString(CultureInfo.InvariantCulture), createPersistentCookie);
    }

    public void LogOff()
    {
      FormsAuthentication.SignOut();
    }
  }
}