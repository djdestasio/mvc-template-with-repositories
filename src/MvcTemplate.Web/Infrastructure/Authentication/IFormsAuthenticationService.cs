﻿namespace MvcTemplate.Web.Infrastructure.Authentication
{
  public interface IFormsAuthenticationService
  {
    void LogOn(int userId, bool createPersistentCookie);
    void LogOff();
  }
}