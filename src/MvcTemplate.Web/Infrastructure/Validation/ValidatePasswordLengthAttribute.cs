﻿#region usings

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;

#endregion

namespace MvcTemplate.Web.Infrastructure.Validation
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
  public sealed class ValidatePasswordLengthAttribute : ValidationAttribute, IClientValidatable
  {
    private const string DefaultErrorMessage = "'{0}' must be at least {1} characters long.";
    private readonly int _minCharacters;

    public ValidatePasswordLengthAttribute(int minCharacters = 6)
      : base(DefaultErrorMessage)
    {
      _minCharacters = minCharacters;
    }

    public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
    {
      yield return new ModelClientValidationStringLengthRule(FormatErrorMessage(metadata.GetDisplayName()), _minCharacters, int.MaxValue);
    }

    public override string FormatErrorMessage(string name)
    {
      return string.Format(CultureInfo.CurrentCulture, ErrorMessageString, name, _minCharacters);
    }

    public override bool IsValid(object value)
    {
      var valueAsString = value as string;
      return string.IsNullOrEmpty(valueAsString) || valueAsString.Length >= _minCharacters;
    }
  }
}