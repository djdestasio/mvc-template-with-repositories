﻿#region usings

using System.Configuration;
using MvcTemplate.Domain.Orm;

#endregion

namespace MvcTemplate.Web.Infrastructure.Orm
{
  public class ConnectionStringProvider : IConnectionStringProvider
  {
    public string GetConnectionString()
    {
      return ConfigurationManager.ConnectionStrings["WindowsAuth"].ConnectionString;
    }
  }
}