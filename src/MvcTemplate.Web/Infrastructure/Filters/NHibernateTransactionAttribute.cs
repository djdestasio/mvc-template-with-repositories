﻿#region usings

using System.Web.Mvc;
using NHibernate;
using StructureMap;

#endregion

namespace MvcTemplate.Web.Infrastructure.Filters
{
  public class NHibernateTransactionAttribute : FilterAttribute, IActionFilter
  {
    public NHibernateTransactionAttribute()
    {
      Order = 0;
    }

    public void OnActionExecuting(ActionExecutingContext filterContext)
    {
      // The reason we cannot inject the ISession is because it is created for each request.
      // A single instance of this attribute may be used to service multiple requests at once
      // and we need to ensure that we are always using the ISession for the current request.
      var nhSession = ObjectFactory.GetInstance<ISession>();
      nhSession.Transaction.Begin();
    }

    public void OnActionExecuted(ActionExecutedContext filterContext)
    {
      var nhSession = ObjectFactory.GetInstance<ISession>();
      if (filterContext.Exception == null && nhSession.Transaction.IsActive)
      {
        nhSession.Transaction.Commit();
      }
    }
  }
}