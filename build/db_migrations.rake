ENV['server'] = "(local)" if !ENV['server']
ENV['database'] = "MvcTemplate" if !ENV['database']

MIGRATE_COMMAND = FileList['packages/TrueFit.Schema.*/tools/tfschemasql.exe'].to_a()[0] + " --server #{ENV['server']} --database #{ENV['database']} --schemaFilePath src/MvcTemplate.Domain/Schema/Schema.xml"

namespace :db do
	desc "Creates an empty database, installs the catalog tables and migrates it to the latest version"
	task :create => [:create_empty_database, :create_catalog_tables, :migrate]
	  
  desc "Migrates the database to the latest version"
  task :migrate do
    sh MIGRATE_COMMAND
  end

  desc "Migrates the database to the newest version using the dryRun option to not commit any changes"
  task :test_migrate do
    sh MIGRATE_COMMAND + " --dryRun"
  end

  desc "Creates the schema catalog tables in the database"
  task :create_catalog_tables do
    sh MIGRATE_COMMAND + " --createCatalogTables"
  end

  desc "Drops the schema catalog tables in the database"
  task :drop_catalog_tables do
    sh MIGRATE_COMMAND + " --dropCatalogTables"
  end

  desc "Creates the catalog tables in the database necessary for the schema tooling to run"
  task :populate_catalog_tables do
    sh MIGRATE_COMMAND + " --catalogStmtsOnly"
  end
end
