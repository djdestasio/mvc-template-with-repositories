AutoMapper Wrapper

This package provides a configuration point for AutoMapper as well as a wrapper
interface that allows for easy mocking.

In App_Start an AutoMapperConfiguration class is added that will initialize
the mapping configuration upon startup of the application. In here you can
put your mapping configurations directory or call outs to other classes
that contain them.

If you have configured an IDependencyResolver, simply set up your backing IoC
container to return an instance of Mapper for when asked for an IMapper.

If you are using the TrueFit Web StructureMap package, in the
StructureMapConfiguration class tell StructureMap to scan the
TrueFit.Web.AutoMapper assembly as there is a registry provided to make the
registration for you.

Example:
s.AssemblyContainingType<IMapper>();
