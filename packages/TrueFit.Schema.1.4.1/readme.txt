Here's a basic rundown of getting started:

A file named Schema.xml located under a Schema folder has been added to your
project. This is where you'll add your schema definitions.

1. Run the Create Catalog Tables.sql script against your database which you'll
find in the tools directory of this package.
2. If you have already created objects in the db which you'd like to include in
the schema file, continue with step 3, else jump to step 5.
3. Run TrueFitSchemaExporter.exe in the tool directory. You can select which objects
you'd like to be exported and the tool will generate a schema file for you.
4. Using either the command line tool (tfschemasql) or the GUI
(TrueFitSchemaSqlGenerator) update the catalog tables in the database to reflect
the objects that already exist. This is done using the --catalogStmtsOnly option
of tfsschemasql or by selecting "Generate catalog statements only" under the
Settings menu in the GUI.
5. Create tables, columns, views, etc. and use either the command line tool
(tfschemasql) or the GUI (TrueFitSchemaSqlGenerator) to migrate your database.

Notes:

There is a file containing Rake tasks to make running the command line tool quick
and easy and to allow for ease of using in any CI builds.

For views, stored procedures, user defined functions and triggers; put the CREATE
statement in the schema file and ensure the name attribute is the same as the name
specified in the CREATE statement. To preserve white space wrap the CREATE statement
in <![CDATA[]]>.

Example:
<storedProcedure name="MySproc">
<![CDATA[
CREATE PROCEDURE MySproc
BEGIN
  SELECT * FROM MyTable
END
]]>
</storedProcedure>
