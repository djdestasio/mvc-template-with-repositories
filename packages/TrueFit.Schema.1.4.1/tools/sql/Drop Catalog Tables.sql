DROP TABLE [TrueFitSchema]
GO
DROP TABLE [TrueFitSchemaTable]
GO
DROP TABLE [TrueFitSchemaTableColumn]
GO
DROP TABLE [TrueFitSchemaComputedTableColumnReference]
GO
DROP TABLE [TrueFitSchemaTableColumnDefault]
GO
DROP TABLE [TrueFitSchemaPrimaryKey]
GO
DROP TABLE [TrueFitSchemaPrimaryKeyColumn]
GO
DROP TABLE [TrueFitSchemaForeignKey]
GO
DROP TABLE [TrueFitSchemaForeignKeyColumn]
GO
DROP TABLE [TrueFitSchemaIndex]
GO
DROP TABLE [TrueFitSchemaIndexColumn]
GO
DROP TABLE [TrueFitSchemaView]
GO
DROP TABLE [TrueFitSchemaStoredProcedure]
GO
DROP TABLE [TrueFitSchemaUserDefinedFunction]
GO
DROP TABLE [TrueFitSchemaTrigger]
GO
DROP TABLE [TrueFitSchemaOneTimeScript]
GO