StructureMap Integration for ASP.NET MVC 3

This package provides dependency injection and extended validation for an
ASP.NET MVC 3 application via StructureMap.

In App_Start a StructureMapConfiguration class is added that will initialize
the container upon startup of the application. There is one TODO in there which
is to tell StructureMap to scan other assemblies in your solution that contain
StructureMap registries that you want to include.

A registry in TrueFit.Web.StructureMap registers a ModelValidatorProvider.
The way the model validation works is that you create a class that inherits
System.Web.Mvc.ModelValidator and that implements
TrueFit.Web.StructureMap.Validation.IModelValidator<T> where T is the type of
model you want to validate. The ModelMetadata and ControllerContext constructor
parameters will be injected automatically along with any other dependencies you
may require. Your class will be automatically picked up by StructureMap and
when posting to an action which includes the model as a parameter your validator
will be called in order do the additional required validation of the model.
